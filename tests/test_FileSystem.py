from __future__ import absolute_import

import filecmp
import itertools
import os
import shutil
import sys
import unittest

from smrm.file_system import FileSystem, FileSystemPathNotFoundError

if sys.version_info[0] == 2:
    from backports import tempfile
else:
    import tempfile


class TestFileSystem(unittest.TestCase):
    """
    Test structure:
    root: directory
        parent_dir_name: directory
            subdir_name: directory
            file_name: file
            symlink_name: symlink to root
            broken_symlink_name: symlink to ??? 
    """
    test_root_dir = None
    parent_dir_name = None
    subdir_name = None
    file_name = None
    symlink_name = None
    broken_symlink_name = None
    bad_path = None
    dirs = []
    files = []
    symlinks = []

    normal_file_system = None
    dry_file_system = None

    def setUp(self):
        self.test_root_dir = tempfile.TemporaryDirectory()

        self.parent_dir_name = os.path.join(self.test_root_dir.name, 'parent_dir')
        os.mkdir(self.parent_dir_name)

        self.subdir_name = os.path.join(self.parent_dir_name, 'subdir_name')
        os.mkdir(self.subdir_name)

        self.file_name = os.path.join(self.parent_dir_name, 'file_name')
        with open(self.file_name, 'w') as f:
            f.write("Hello, world!")

        self.symlink_name = os.path.join(self.parent_dir_name, 'symlink_name')
        os.symlink(self.test_root_dir.name, self.symlink_name)

        self.broken_symlink_name = os.path.join(self.test_root_dir.name, 'broken_symlink')
        os.symlink(os.path.join(self.test_root_dir.name, "path_to_nonexist_object"), self.broken_symlink_name)

        self.bad_path = os.path.join(self.test_root_dir.name, 'bad_path')

        self.normal_file_system = FileSystem(dry_mode=False)
        self.dry_file_system = FileSystem(dry_mode=True)

        self.dirs = [self.test_root_dir.name, self.parent_dir_name, self.subdir_name]
        self.files = [self.file_name]
        self.symlinks = [self.symlink_name, self.broken_symlink_name]

    def tearDown(self):
        self.test_root_dir.cleanup()

    def test_norm_is_directory(self):
        for dir_path in self.dirs:
            self.assertTrue(self.normal_file_system.is_directory(dir_path))

        for non_dirs_path in itertools.chain(self.files, self.symlinks):
            self.assertFalse(self.normal_file_system.is_directory(non_dirs_path))

    def test_norm_is_symlink(self):
        for symlinks in self.symlinks:
            self.assertTrue(self.normal_file_system.is_symlink(symlinks))

        for non_symlinks in itertools.chain(self.files, self.dirs):
            self.assertFalse(self.normal_file_system.is_symlink(non_symlinks))

    def test_norm_is_file(self):
        for file in self.files:
            self.assertTrue(self.normal_file_system.is_file(file))

        for non_file in itertools.chain(self.dirs, self.symlinks):
            self.assertFalse(self.normal_file_system.is_file(non_file))

    def test_norm_path_exists(self):
        for obj in itertools.chain(self.dirs, self.files, self.symlinks):
            self.assertTrue(self.normal_file_system.path_exists(obj))

            # test of parent dir, it is always ok if above object exists
            self.assertTrue(self.normal_file_system.path_exists(os.path.dirname(obj)))

            # always fails (may be)
            self.assertFalse(self.normal_file_system.path_exists(obj + "bad_path"))

    def test_absolute_path(self):
        working_directory = os.getcwd()
        self.assertEqual(
            FileSystem.absolute_path(working_directory),
            working_directory)
        self.assertEqual(
            FileSystem.absolute_path('123'),
            os.path.join(working_directory, '123'))

    def test_norm_remove(self):
        self.normal_file_system.remove(self.parent_dir_name)
        self.assertTrue(os.path.exists(self.test_root_dir.name))  # is deleted root dir from symlink?
        self.assertFalse(os.path.exists(self.parent_dir_name))  # is removed?

    def test_norm_copy(self):
        src = self.parent_dir_name
        with tempfile.TemporaryDirectory(dir=self.test_root_dir.name) as dst_dir:
            dst = dst_dir

        self.normal_file_system.copy(src, dst)

        self.assertTrue(os.path.exists(src))
        self.assertTrue(os.path.exists(dst))
        self.assertTrue(os.path.isdir(dst))

        self.assertSequenceEqual(filecmp.dircmp(src, dst).diff_files, [])

    def test_norm_move(self):  # todo test moving to another device. How to do it?
        src = self.parent_dir_name
        with tempfile.TemporaryDirectory() as b:
            backup_dir = b
        shutil.copytree(src, backup_dir, symlinks=True)
        with tempfile.TemporaryDirectory() as dst:
            os.rmdir(dst)
            self.normal_file_system.move(src, dst)
            self.assertSequenceEqual(filecmp.dircmp(dst, backup_dir).diff_files, [])
            with self.assertRaises(FileSystemPathNotFoundError):
                self.normal_file_system.move(src, dst)

    def test_norm_get_object_type(self):
        for dir in self.dirs:
            self.assertEqual(
                self.normal_file_system.get_object_type(dir), 'directory')

        for file in self.files:
            self.assertEqual(
                self.normal_file_system.get_object_type(file), 'file')

        for symlink in self.symlinks:
            self.assertEqual(
                self.normal_file_system.get_object_type(symlink), 'symlink')

        self.assertIsNone(self.normal_file_system.get_object_type(self.bad_path))

    def test_join_path(self):
        self.assertEqual(
            FileSystem.join_path('123', '456'),
            os.path.join('123', '456')
        )

        # if subpath is absolute path, return it
        self.assertEqual(
            FileSystem.join_path('123', os.getcwd()),
            os.getcwd()
        )

    def test_make_dir(self):
        with tempfile.TemporaryDirectory() as test_dir_name:  # prevent file or dir exists
            dir_name_to_create = os.path.join(os.path.join(test_dir_name, 'test_name'), 'another')

            self.normal_file_system.make_dir(dir_name_to_create)
            self.assertTrue(os.path.exists(dir_name_to_create))

            with self.assertRaises(OSError):
                self.normal_file_system.make_dir(dir_name_to_create)
            self.assertTrue(os.path.exists(dir_name_to_create))

            self.normal_file_system.make_dir(dir_name_to_create, exist_ok=True)
            self.assertTrue(os.path.exists(dir_name_to_create))

    def test_parent_dir(self):
        self.assertEqual(
            self.normal_file_system.parent_dir(self.subdir_name),
            self.parent_dir_name
        )

        self.assertEqual(
            self.normal_file_system.parent_dir(self.parent_dir_name),
            self.test_root_dir.name
        )

    def test_relative_path(self):
        pass

    def test_norm_walk(self):
        pass

    def test_norm_rename(self):
        pass

    def test_norm_read_symlink(self):
        pass

    def test_norm_make_symlink(self):
        pass

    def test_norm_unlink(self):
        pass


if __name__ == '__main__':
    unittest.main()
