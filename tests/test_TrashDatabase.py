import os
import sys
import unittest

from smrm.trash.trash_database import TrashDatabase

if sys.version_info[0] == 2:
    from backports import tempfile
else:
    import tempfile


# todo write tests
class TestTrashDatabase(unittest.TestCase):
    def setUp(self):
        self.root_dir = tempfile.TemporaryDirectory()
        self.db_name = os.path.join(self.root_dir.name, 'db.sqlite')
        self.trash_db = TrashDatabase(self.db_name)

    def tearDown(self):
        pass
