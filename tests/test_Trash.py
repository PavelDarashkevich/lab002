import filecmp
import os
import shutil
import sys
import time
import unittest
from datetime import datetime
from filecmp import dircmp

import itertools

from smrm.user_questions import UserQuestions

from smrm.actions import FileSystemActions
from smrm.config.config import Config, ActionMode, OutputMode
from smrm.file_system import FileSystem
from smrm.trash.trash import Trash, TrashProtectedPathError
from smrm.trash.trash_database import TrashDatabase

if sys.version_info[0] == 2:
    from backports import tempfile
else:
    import tempfile


class TestTrash(unittest.TestCase):
    def _init_trash(self, db_path, trash_path, restore_path):
        self.database = TrashDatabase(db_path)
        self.database.repair()

        self.trash_info = [
            {
                'id': 15,
                'name_in_trash': 'F1LE',
                'path_to_restore': os.path.join(restore_path, 'hfg'),
                'type': 'file',
                'data': '13425465',
                'removed_at': int(time.time())
            },
            {
                'id': 125,
                'name_in_trash': 'SML1NK',
                'path_to_restore': os.path.join(restore_path, 'hfg2'),
                'type': 'symlink',
                'data': trash_path,
                'removed_at': int(time.mktime(datetime(1, 3, 4).timetuple()))
            },
            {
                'id': 126,
                'name_in_trash': 'D1R',
                'path_to_restore': os.path.join(restore_path, 'hfg3'),
                'type': 'directory',
                'files': [
                    {"name_in_trash": 'file', 'type': 'file', 'data': '134256'},
                    {"name_in_trash": 'dir', 'type': 'directory', 'files': []},
                    {"name_in_trash": 'symlink', 'type': 'symlink', 'data': '345'}
                ],
                'removed_at': int(time.mktime(datetime(2017, 2, 1).timetuple()))
            },
            {
                'id': 2333,
                'name_in_trash': 'EMPTY_DIR',
                'path_to_restore': os.path.join(restore_path, 'EMPTY_DIR'),
                'type': 'directory',
                'files': [],
                'removed_at': int(time.mktime(datetime(2017, 2, 1).timetuple()))
            }
        ]
        self.trash = Trash(trash_path=trash_path, file_system=self.file_system)

        for obj in self.trash_info:
            self.database.create(id=obj['id'],
                                 name_in_trash=obj['name_in_trash'],
                                 path_to_restore=obj['path_to_restore'],
                                 removed_at=obj['removed_at'])

        self._init_dir(trash_path, self.trash_info)

    def setUp(self):
        config = Config()
        config.action_mode = ActionMode.FAIL
        config.output_mode = OutputMode.NO_OUTPUT

        file_system = FileSystem(dry_mode=False)
        user_questions = UserQuestions(config)
        self.file_system = FileSystemActions(config, file_system, user_questions)

        self._root_dir = tempfile.TemporaryDirectory()

        self.trash_path = os.path.join(self._root_dir.name, 'trash')
        self.restore_path = os.path.join(self._root_dir.name, 'restore')
        self.backup_path = os.path.join(self._root_dir.name, 'backup')

        self.trash_db_path = os.path.join(self.trash_path, 'db.sqlite')
        self._init_trash(self.trash_db_path, self.trash_path,
                         self.restore_path)

    def tearDown(self):
        self._root_dir.cleanup()

    def _init_dir(self, trash_path, trash_info):
        for obj in trash_info:
            obj_path = os.path.join(trash_path, obj['name_in_trash'])
            if obj['type'] == 'file':
                with open(obj_path, 'w') as f:
                    f.write(obj['data'])
            elif obj['type'] == 'symlink':
                os.symlink(obj['data'], obj_path)
            elif obj['type'] == 'directory':
                os.mkdir(obj_path)
                self._init_dir(obj_path, obj['files'])

    def test_move_object(self):
        with self.assertRaises(TrashProtectedPathError):
            self.trash.move_object(self.trash_path)

        self.file_system.copy(self.trash_path, self.restore_path)
        self.file_system.copy(self.restore_path, self.backup_path)
        for obj in self.trash_info:
            path_to_moved_object = self.file_system.join_path(self.restore_path,
                                                              obj['name_in_trash'])

            objects_before = list(self.database.get_all())
            self.assertTrue(self.file_system.path_exists(path_to_moved_object))
            descriptor = self.trash.move_object(path_to_moved_object)
            self.assertFalse(self.file_system.path_exists(path_to_moved_object))

            objects_after = list(self.database.get_all())
            objects_diff = [o for o in itertools.chain(objects_before, objects_after) if
                            (o in objects_before) ^ (o in objects_after)]
            self.assertEqual(len(objects_diff), 1)
            object_info = objects_diff.pop()

            self.assertEqual(object_info['id'], descriptor)
            backup_obj_path = self.file_system.join_path(self.backup_path, obj['name_in_trash'])
            trash_obj_path = self.file_system.join_path(self.trash_path, object_info['name_in_trash'])
            if obj['type'] == 'directory':
                self.assertSequenceEqual(dircmp(
                    backup_obj_path, trash_obj_path
                ).diff_files, [])
            elif obj['type'] == 'file':
                self.assertTrue(
                    filecmp.cmp(backup_obj_path, trash_obj_path))
            elif obj['type'] == 'symlink':
                self.assertEqual(os.readlink(backup_obj_path), os.readlink(trash_obj_path))
            else:
                raise TypeError(obj['type'])

    def test_restore_file(self):
        for obj in self.trash_info:
            abs_path_in_trash = self.file_system.join_path(
                self.trash.trash_path,
                obj['name_in_trash']
            )
            self.assertFalse(self.file_system.path_exists(obj['path_to_restore']))
            self.assertTrue(self.file_system.path_exists(abs_path_in_trash))

            if obj['type'] == 'directory' and len(obj['files']) != 0:
                name = obj['files'][0]['name_in_trash']

                shutil.copytree(self.trash_path, self.backup_path, symlinks=True)

                user_path = "{id}/{name}".format(
                    id=obj['id'], name=name)
                path_in_backup = "{backup}/{dir}/{name}".format(backup=self.backup_path,
                                                                dir=obj['name_in_trash'],
                                                                name=name)
                path_in_restore = os.path.join(obj['path_to_restore'], name)

                self.trash.restore_file(user_path)
                self.assertTrue(filecmp.cmp(
                    path_in_backup,
                    path_in_restore
                ), [path_in_backup, path_in_restore])

                self.file_system.remove(self.restore_path)
                self.file_system.remove(self.backup_path)

            self.trash.restore_file(str(obj['id']))

            self.assertTrue(self.file_system.path_exists(obj['path_to_restore']))
            self.assertFalse(self.file_system.path_exists(abs_path_in_trash))

    def test_erase_object(self):
        for obj in self.trash_info:
            old_descriptors = set(self.trash.get_descriptors())
            self.trash.erase_object(str(obj['id']))
            new_descriptors = set(self.trash.get_descriptors())
            self.assertSetEqual(old_descriptors.symmetric_difference(new_descriptors),
                                {str(obj['id'])})
            self.assertFalse(self.file_system.path_exists(
                self.file_system.join_path(self.trash.trash_path, obj['name_in_trash'])
            ))

    def test_object_info(self):
        for obj in self.trash_info:
            answer = self.trash.object_info(str(obj['id']))
            self.assertEqual(answer['descriptor'], str(obj['id']))
            # self.assertEqual(answer['path_inside'], None)
            self.assertEqual(answer['path_to_restore'], obj['path_to_restore'])
            self.assertEqual(answer['path_in_trash'], obj['name_in_trash'])
            self.assertEqual(answer['absolute_path_in_trash'],
                             os.path.join(self.trash.trash_path, obj['name_in_trash']))
            self.assertEqual(answer['removed_at'], obj['removed_at'])
            self.assertEqual(answer['type'], obj['type'])

            if obj['type'] == 'directory':
                pass  # todo write this

    def test_view_objects(self):
        pass

    def test_repair(self):
        pass

    def test_get_descriptors(self):
        descriptors = [str(item['id']) for item in self.trash_info]
        self.assertSequenceEqual(descriptors, self.trash.get_descriptors())
        self.assertEqual(len(set(self.trash.get_descriptors())),
                         len(self.trash.get_descriptors()))

    def test__split_by_descriptor(self):
        cases = {
            "234": ("234", None),
            "123/": ("123", ""),
            "descriptor/path\\to/file": ("descriptor", "path\\to/file"),
            "descriptor\\path/to/file": ("descriptor", "path/to/file"),
            "   ": (None, None),
            None: (None, None)
        }

        for path, answer in cases.items():
            self.assertEqual(self.trash._split_by_descriptor(path), answer)

    def test_get_new_name_in_trash(self):
        pass

    def test__convert_path(self):
        for obj in self.trash_info:
            self.assertEqual(obj['name_in_trash'], self.trash._convert_path(str(obj['id'])))

            if obj['type'] == 'directory':
                for sub_obj in obj['files']:
                    name = "{id}{sep}{name}".format(id=obj['id'],
                                                    sep=os.sep,
                                                    name=sub_obj['name_in_trash'])
                    self.assertEqual(os.path.join(obj['name_in_trash'], sub_obj['name_in_trash']),
                                     self.trash._convert_path(name))

    def test_get_random_descriptor(self):
        descriptors = set()
        attempts = 10
        for i in range(attempts):
            descriptors.add(self.trash.get_random_descriptor())

        self.assertEqual(len(descriptors), attempts)


if __name__ == '__main__':
    unittest.main()
