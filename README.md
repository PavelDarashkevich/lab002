## Smart rm

Advanced remove files and directories utility for Linux

### Install:
1. Clone this repository
2. ???
3. Profit!

### Usage:
	smrm [OPTIONS]... [FILE]
	-d, --directory
              unlink  directory,  even  if  non-empty

    -f, --force
              ignore nonexistent files, never prompt

    -i, --interactive
              prompt before any removal

    -r, -R, --recursive
              remove the contents of directories recursively

    --restore
              restore files

    -v, --verbose
              explain what is being done

    --help display this help and exit

    --version
              output version information and exit

### Features:
1. Similar to `rm` command line syntax
	- remove files, directories
	
2. Trash support
	- view, restore and finally remove files
	- manual and automatic empty trash

3. Flexible configuration file
	- trash location
	- empty policing
		- by max file size
		- by time in trash
		- combined rules

4. Safe work:
	- warnings if try to remove system directory or many files
	- detect cycles - OK
	
### Requirements:
1. Run as `rm` - OK
2. Support using as module - OK
3. All configuration params may be parsed in command line (also config)
4. Default configuration file for default user 
5. Using argparse - OK
6. Module architecture - OK
7. Conflict resolving policy and choices in config
8. Check free disk space before operation
9. Dry-run mode
10. Silent mode - OK
11. Progress of task, description after finish operation
12. Logging - OK
13. JSON and handwritten format for config
14. Unit testing of main features
14. Installing with `setup.py` - OK

