#!/usr/bin/env python
from setuptools import setup, find_packages
import sys

from setuptools.command.install import install

install_requires = ['peewee', 'tabulate', 'enum34']


class SmrmInstall(install):
    def run(self):
        install.run(self)

if sys.version_info[0] == 2:
    install_requires.append('backports.tempfile')

setup(
    # cmdclass={
    #     'install': SmrmInstall
    # },
    name='smrm',
    packages=find_packages(), install_requires=install_requires,
    entry_points={
        'console_scripts': ['smrm = smrm.program.program:main']
    }
)
