from abc import abstractmethod


class Observable(object):
    @abstractmethod
    def update(self, *args, **kwargs):
        pass


class Observer(object):
    def __init__(self):
        self._registered = set()

    def register(self, obj):
        self._registered.add(obj)

    def unregister(self, obj):
        self._registered.remove(obj)

    def notify(self, *args, **kwargs):
        for obj in self._registered:
            obj.update(*args, **kwargs)
