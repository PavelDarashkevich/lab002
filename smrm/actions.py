from __future__ import print_function
import logging
import re

import pathos

from smrm.config.config import Config, OutputMode
from smrm.file_system import *
from smrm.user_questions import UserAnswer, Action


class UserAbortError(Exception):
    def __init__(self):
        pass


class FileSystemActions(object):
    def __init__(self, config, file_system, user_questions, dry_run=False):
        """
        :type config Config
        :type file_system FileSystem
        :param config:
        :param file_system:
        :param user_questions
        """
        self.config = config
        self.file_system = file_system
        self.user_questions = user_questions
        self.dry_mode = dry_run
        self.path_separator = self.file_system.path_separator
        #
        self.name_filter = lambda name: (
            re.search(getattr(config, 'regex_filter', ''), name) is not None)

    def _dst_path(self, src, dst, current_src_path):
        relative_path = self.file_system.relative_path(current_src_path, src)
        if relative_path == '.':
            return dst
        return self.file_system.join_path(dst, relative_path)

    def _scan_iter(self, path,
                   on_file=None,
                   on_symlink=None,
                   on_descend=lambda **kwargs: True,
                   on_emptydir_match=None,
                   on_listdir=None,
                   meta=None,
                   name_filter=lambda name: True):
        """
        :param on_file:
        :param on_symlink:
        :param on_descend:
        :param on_emptydir_match:
        :param path:
        :return:
        """

        if not self.file_system.path_exists(path):
            raise FileSystemPathNotFoundError(path)

        if self.file_system.is_file(path):
            try:
                if on_file and name_filter(self.file_system.base_name(path)):
                    on_file(path, meta=meta)
            except Exception as e:
                raise e
        elif self.file_system.is_symlink(path):
            if on_symlink and name_filter(self.file_system.base_name(path)):
                on_symlink(path, meta=meta)
        elif self.file_system.is_directory(path):
            if on_descend(path, meta=meta):
                names = on_listdir(path, meta)

                scan_kwargs = {
                    'on_file': on_file,
                    'on_symlink': on_symlink,
                    'on_descend': on_descend,
                    'on_emptydir_match': on_emptydir_match,
                    'on_listdir': on_listdir,
                    'meta': meta,
                    'name_filter': name_filter
                }

                if name_filter(self.base_name(path)):
                    scan_kwargs['name_filter'] = lambda x: True

                new_paths = []
                for name in names:
                    new_path = self.file_system.join_path(path, name)

                    new_paths.append(new_path)

                ret = {
                    'new_paths': new_paths,
                    'scan_kwargs': scan_kwargs}

                if name_filter(self.base_name(path)):
                    ret['callback'] = [on_emptydir_match, [path], {'meta': meta}]

                return ret

        else:
            raise FileSystemInvalidObjectError(path)

    def _scan(self, path,
              on_file=None,
              on_symlink=None,
              on_emptydir_match=None,
              on_descend=lambda **kwargs: True,
              on_listdir=None,
              meta=None,
              name_filter=lambda name: True
              ):

        scan_kwargs = {
            'on_file': on_file,
            'on_symlink': on_symlink,
            'on_descend': on_descend,
            'on_emptydir_match': on_emptydir_match,
            'on_listdir': on_listdir,
            'meta': meta,
            'name_filter': name_filter
        }

        pool = pathos.multiprocessing.Pool()
        options = [{'args': [path], 'kwargs': scan_kwargs}]

        callbacks = []
        while len(options) != 0:
            futures = []
            for option in options:
                futures.append(pool.apply_async(self._scan_iter, args=option['args'], kwds=option['kwargs']))

            options = []
            for future in futures:
                result = future.get()
                if result is not None:
                    new_paths = result['new_paths']
                    callback = result.get('callback')
                    callbacks.append(callback)
                    for new_path in new_paths:
                        options.append({'args': [new_path], 'kwargs': result['scan_kwargs']})

        callbacks.reverse()
        for callback in callbacks:
            if callback is not None:
                callback[0](*callback[1], **callback[2])

    def move(self, src, dst):
        """
        :param src:
        :param dst:
        :return:
        """

        return self._scan(path=src,
                          meta={'src': src, 'dst': dst},
                          on_file=self.on_move,
                          on_symlink=self.on_move,
                          on_descend=self.on_descend_into,
                          on_emptydir_match=self.on_move_empty,
                          on_listdir=self.on_listdir,
                          name_filter=self.name_filter)

    def copy(self, src, dst):
        """
        :param src:
        :param dst:
        :return:
        """

        return self._scan(path=src,
                          meta={'src': src, 'dst': dst},
                          on_file=self.on_copy,
                          on_symlink=self.on_copy,
                          on_descend=self.on_descend_into,
                          on_emptydir_match=self.on_copy_empty,
                          on_listdir=self.on_listdir)

    def remove(self, src):
        """
        :param src:
        :return:
        """

        return self._scan(path=src,
                          meta=None,
                          on_file=self.on_remove,
                          on_symlink=self.on_remove,
                          on_descend=self.on_descend_into,
                          on_emptydir_match=self.on_remove_empty_dirs,
                          on_listdir=self.on_listdir,
                          name_filter=self.name_filter)

    def on_remove_empty_dirs(self, path, meta=None):
        while True:
            try:
                if self.dry_mode:
                    return

                try:
                    return self.file_system.remove_empty_dir(path)
                except FileSystemDirNotEmpty:
                    return
            except FileSystemPermissionDeniedError as e:
                user_answer = self.user_questions.permission_denied(Action.REMOVE, path)
                if user_answer == UserAnswer.ABORT:
                    raise e
                elif user_answer == UserAnswer.SKIP:
                    return
                else:
                    continue

    def on_listdir(self, path, meta):
        while True:
            try:
                return self.file_system.listdir(path)
            except FileSystemPermissionDeniedError as e:
                user_answer = self.user_questions.permission_denied(Action.LIST_DIR, path)
                if user_answer == UserAnswer.ABORT:
                    raise e
                elif user_answer == UserAnswer.SKIP:
                    return []
                else:
                    continue

    def on_move(self, src, meta):
        dst = self._dst_path(meta['src'], meta['dst'], src)
        while True:
            try:
                if self.file_system.is_directory(src):
                    should_remove_empty_dirs = getattr(self.config, 'move_empty_dirs', True)
                    is_recursive = getattr(self.config, 'recursive', True)

                    if not is_recursive:
                        if not should_remove_empty_dirs:
                            raise FileSystemIsADirectoryError(src)
                        if should_remove_empty_dirs and len(self.file_system.listdir(src)) != 0:
                            raise FileSystemDirNotEmpty(src)

                user_answer = self.user_questions.move_to_trash(Action.MOVE, src)
                if user_answer == UserAnswer.ABORT:
                    raise UserAbortError
                if user_answer == UserAnswer.SKIP:
                    return
                if user_answer == UserAnswer.YES:
                    if not self.dry_mode:
                        self.file_system.move(src, dst)
                    if self.config.output_mode <= OutputMode.ALL_INFO:
                        logging.info("'%s' -> '%s'", src, dst)
                    return
            except FileSystemPathExistsError as e:
                is_directories = self.file_system.is_directory(src) and \
                                 self.file_system.is_directory(dst)

                user_answer = self.user_questions.path_exists(Action.MOVE, src, dst, is_directories)
                if user_answer == UserAnswer.ABORT:
                    raise e
                elif user_answer == UserAnswer.SKIP:
                    return
                elif user_answer == UserAnswer.REPLACE:
                    self.remove(e.path)
                    continue
                elif user_answer == UserAnswer.MERGE:
                    names = self.on_listdir(src, meta)
                    for name in names:
                        self.on_move(self.file_system.join_path(src, name), meta)

                    try:
                        self.file_system.remove_empty_dir(src)
                    except OSError:
                        pass

                    return
                elif user_answer == UserAnswer.RENAME:
                    new_name = self.user_questions.new_name(Action.MOVE, src)
                    if new_name == UserAnswer.ABORT:
                        continue

                    dst = self.file_system.join_path(self.file_system.parent_dir(dst), new_name)
                    continue
                else:  # retry or other
                    continue
            except FileSystemPermissionDeniedError as e:
                user_answer = self.user_questions.permission_denied(Action.MOVE, src)
                if user_answer == UserAnswer.ABORT:
                    raise e
                elif user_answer == UserAnswer.SKIP:
                    return
                else:
                    continue

    def on_move_empty(self, src, meta):
        dst = self._dst_path(meta['src'], meta['dst'], src)

        self.file_system.make_dir(dst, exist_ok=True)
        try:
            self.file_system.remove_empty_dir(src)
        except Exception:
            pass

    def on_copy_empty(self, src, meta):
        dst = self._dst_path(meta['src'], meta['dst'], src)

        self.file_system.make_dir(dst, exist_ok=True)

    def on_copy(self, src, meta):
        dst = self._dst_path(meta['src'], meta['dst'], src)
        while True:
            try:
                if self.file_system.is_directory(src):
                    should_remove_empty_dirs = getattr(self.config, 'move_empty_dirs', True)
                    is_recursive = getattr(self.config, 'recursive', True)

                    if not is_recursive:
                        if not should_remove_empty_dirs:
                            raise FileSystemIsADirectoryError(src)
                        if should_remove_empty_dirs and len(self.file_system.listdir(src)) != 0:
                            raise FileSystemDirNotEmpty(src)

                user_answer = self.user_questions.move_to_trash(Action.MOVE, src)
                if user_answer == UserAnswer.ABORT:
                    raise UserAbortError
                if user_answer == UserAnswer.SKIP:
                    return
                if user_answer == UserAnswer.YES:
                    if not self.dry_mode:
                        self.file_system.copy(src, dst)
                    if self.config.output_mode <= OutputMode.ALL_INFO:
                        logging.info("'%s' -> '%s'", src, dst)
                    return
            except FileSystemPathExistsError as e:
                is_directories = self.file_system.is_directory(src) and \
                                 self.file_system.is_directory(dst)

                user_answer = self.user_questions.path_exists(Action.MOVE, src, dst, is_directories)
                if user_answer == UserAnswer.ABORT:
                    raise e
                elif user_answer == UserAnswer.SKIP:
                    return
                elif user_answer == UserAnswer.REPLACE:
                    self.remove(e.path)
                    continue
                elif user_answer == UserAnswer.MERGE:
                    names = self.on_listdir(src, meta)
                    for name in names:
                        self.on_copy(self.file_system.join_path(src, name), meta)
                    return
                elif user_answer == UserAnswer.RENAME:
                    new_name = self.user_questions.new_name(Action.MOVE, src)
                    if new_name == UserAnswer.ABORT:
                        continue

                    dst = self.file_system.join_path(self.file_system.parent_dir(dst), new_name)
                    continue
                else:  # retry or other
                    continue
            except FileSystemPermissionDeniedError as e:
                user_answer = self.user_questions.permission_denied(Action.MOVE, src)
                if user_answer == UserAnswer.ABORT:
                    raise e
                elif user_answer == UserAnswer.SKIP:
                    return
                else:
                    continue

    def on_remove(self, src, meta):
        while True:
            try:
                if not self.dry_mode:
                    self.file_system.remove(src)

                if self.config.output_mode <= OutputMode.ALL_INFO:
                    logging.info("erased: '%s'", src)
                return
            except FileSystemPermissionDeniedError as e:
                user_answer = self.user_questions.permission_denied(Action.REMOVE, src)
                if user_answer == UserAnswer.ABORT:
                    raise UserAbortError
                elif user_answer == UserAnswer.SKIP:
                    return []
                else:
                    continue

    def on_descend_into(self, path, meta):
        should_remove_empty_dirs = getattr(self.config, 'move_empty_dirs', True)
        is_recursive = getattr(self.config, 'recursive', True)

        if not is_recursive:
            if not should_remove_empty_dirs:
                raise FileSystemIsADirectoryError(path)
            if should_remove_empty_dirs and len(self.file_system.listdir(path)) != 0:
                raise FileSystemDirNotEmpty(path)

        user_answer = self.user_questions.descend_into(Action.DESCEND_INTO, path)
        if user_answer == UserAnswer.ABORT:
            raise UserAbortError
        if user_answer == UserAnswer.YES:
            return True
        if user_answer == UserAnswer.NO:
            return False

        return NotImplementedError

    def path_exists(self, path):
        return self.file_system.path_exists(path)

    @staticmethod
    def join_path(path, subpath):
        return FileSystem.join_path(path, subpath)

    @staticmethod
    def base_name(path):
        return FileSystem.base_name(path)

    def get_object_type(self, path):
        return self.file_system.get_object_type(path)

    def absolute_path(self, path):
        return self.file_system.absolute_path(path)

    def listdir(self, path):
        return self.file_system.listdir(path)