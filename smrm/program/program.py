# coding=utf-8
import logging
import sys
import traceback

from smrm.actions import FileSystemActions, FileSystemPermissionDeniedError, UserAbortError
from smrm.arguments.arguments_parser import ArgumentsParser
from smrm.config.config import OutputMode
from smrm.config.config_loader import ConfigLoader
from smrm.file_system import FileSystemPathNotFoundError, FileSystemPathExistsError, \
    FileSystemInvalidObjectError, FileSystemDirNotEmpty, FileSystemIsADirectoryError, FileSystem
from smrm.program.modes.autoremove import autoremove_mode
from smrm.program.modes.erase import erase_mode
from smrm.program.modes.repair import repair_mode
from smrm.program.modes.restore import restore_mode
from smrm.program.modes.trash import trash_mode, TrashPoliticsError
from smrm.program.modes.view import view_mode
from smrm.trash.trash import Trash, TrashDescriptorNotFoundError, TrashCorruptedError, \
    TrashProtectedPathError
from smrm.user_questions import UserQuestions


def init_logging(config):
    output_mode_dict = {
        OutputMode.DEBUG: logging.DEBUG,
        OutputMode.ALL_INFO: logging.INFO,
        OutputMode.MAIN_INFO: logging.INFO,
        OutputMode.WARNING: logging.WARNING,
        OutputMode.ERROR: logging.ERROR,
        OutputMode.CRITICAL: logging.CRITICAL,
        OutputMode.NO_OUTPUT: logging.NOTSET
    }

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)

    file_formatter = logging.Formatter(
        "%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    file_handler = logging.FileHandler(config.log_path)
    file_handler.setFormatter(file_formatter)
    file_handler.setLevel(logging.DEBUG)
    root_logger.addHandler(file_handler)

    console_formatter = logging.Formatter("smrm:%(levelname)s: %(message)s")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(console_formatter)
    console_handler.setLevel(output_mode_dict[config.output_mode])
    if config.output_mode != OutputMode.NO_OUTPUT:
        root_logger.addHandler(console_handler)


def main():
    if sys.version_info[0] == 2:
        reload(sys)
        sys.setdefaultencoding('utf8')

    args = ArgumentsParser().parse_arguments()
    config = ConfigLoader().load(args)

    user_questions = UserQuestions(config)

    file_system = FileSystem(dry_mode=config.dry_mode)
    file_system_actions = FileSystemActions(config, file_system, user_questions,
                                            dry_run=config.dry_mode)

    init_logging(config)
    trash = Trash(trash_path=config.trash_path,
                  file_system=file_system_actions,
                  protected_paths=config.protected_paths)

    try:
        if args.action_mode == 'trash':
            trash_mode(config, args.paths, trash)
        elif args.action_mode == 'restore':
            if args.all:
                descriptors = trash.get_descriptors()
            else:
                descriptors = args.descriptors

            restore_mode(config, descriptors, trash)
        elif args.action_mode == 'view':
            if args.all:
                paths = trash.get_descriptors()
            else:
                paths = args.paths

            view_mode(config, paths, trash)
        elif args.action_mode == 'erase':
            if args.all:
                descriptors = trash.get_descriptors()
            else:
                descriptors = args.descriptors

            erase_mode(config, descriptors, trash)
        elif args.action_mode == 'repair':
            repair_mode(config, trash)
        elif args.action_mode == 'autoremove':
            autoremove_mode(config, trash)
        else:
            logging.critical("Invalid mode: %s", args.action_mode)
            logging.debug("Arguments: %s\nsys.argv: %s", args, sys.argv)
            exit(1)

    except UserAbortError:
        logging.error("Aborted!")
    except FileSystemPathNotFoundError as e:
        logging.error("Path not found: '%s'", e.path)
    except FileSystemPathExistsError as e:
        logging.error("Path exists: '%s'", e.path)
    except FileSystemInvalidObjectError as e:
        logging.error("Invalid object: '%s' isn't a dir, file or symlink", e.path)
    except FileSystemDirNotEmpty as e:
        logging.error("Directory %s isn't empty", e.path)
    except FileSystemIsADirectoryError as e:
        logging.error("Path %s is a directory", e.path)
    except FileSystemPermissionDeniedError as e:
        logging.error("Permission denied: %s", e.path)
    # Trash exceptions
    except TrashDescriptorNotFoundError as e:
        logging.error("Descriptor %s doesn't found in the trash", e.descriptor)
    except TrashCorruptedError as e:
        logging.critical("Trash corrupted! Descriptor %s is disappeared!", e.descriptor)
    except TrashProtectedPathError as e:
        logging.critical("Attempt to remove protected path: %s", e.path)

    except TrashPoliticsError as e:
        logging.error(e.message)

    except Exception as e:
        logging.critical("Unhandled exception: name: %s, exception: %s", type(e), e)
        logging.debug(traceback.format_exc())
        exit(255)
