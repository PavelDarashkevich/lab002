import logging

from smrm.config.config import Config, OutputMode
from smrm.trash.trash import Trash


def repair_mode(config, trash):
    """
    :type config Config
    :type trash Trash
    :param config: 
    :param trash: 
    :return: 
    """
    repair_info = trash.repair()

    if config.output_mode <= OutputMode.MAIN_INFO:
        logging.info('Fixed broken rows: %d', len(repair_info['broken_rows']))
        if len(repair_info['broken_rows']) != 0 and config.output_mode <= OutputMode.ALL_INFO:
            for row in repair_info['broken_rows']:
                print("\t" + str(row))

        logging.info('Lost objects in trash: %d', len(repair_info['lost_objects']))
        if len(repair_info['lost_objects']) != 0 and config.output_mode <= OutputMode.ALL_INFO:
            for name in repair_info['lost_objects']:
                print("\t" + str(name))
