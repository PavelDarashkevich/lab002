# coding=utf-8
import datetime
import logging
import re

from tabulate import tabulate


def get_files(file_system, path, depth_level=0, max_depth=-1,
              is_allow_file=lambda name, full_path: True):
    if file_system.dry_mode:
        raise NotImplementedError()

    parent_dir, name = file_system.split(path)
    files = []
    file_info = {
        'name': name,
        'path': path,
        'type': file_system.get_object_type(path),
        'depth': depth_level,
        'status': 'ok'
    }

    new_files = []
    if file_system.is_directory(path):
        names_in_dir = []
        has_error = False
        try:
            names_in_dir = file_system.listdir(path)
        except OSError:
            file_info['status'] = 'permission_denied'
            has_error = True

        if not has_error:
            if depth_level == max_depth and len(names_in_dir) != 0:
                file_info['status'] = 'depth_limit_exceeded'
            else:
                for obj in names_in_dir:
                    new_files += get_files(
                        file_system,
                        file_system.join_path(path, obj),
                        depth_level=depth_level + 1,
                        max_depth=max_depth,
                        is_allow_file=is_allow_file
                    )

    if is_allow_file(name, path) or len(new_files) != 0:
        files.append(file_info)

    files += new_files

    return files


def pack_in_row(depth_level=0, user_path=None, path_to_restore=None, removed_at=None,
                object_type=None):
    return [user_path, '│    ' * depth_level + (path_to_restore or ""), removed_at, object_type]


def dir_not_empty_row(depth_level):
    return pack_in_row(depth_level=depth_level, path_to_restore="---dir not empty---")


def permission_denied_row(depth_level):
    return pack_in_row(depth_level=depth_level, path_to_restore="---permission denied---")


def dir_end(depth_level):
    return pack_in_row(depth_level=depth_level)


def view_mode(config, paths, trash):
    """
    :type config smrm.config.ViewConfig
    :type paths list
    :type trash Trash
    
    :param config: 
    :param trash: 
    :return: 
    """
    objs = trash.view_objects(paths=paths)

    if len(objs) == 0:
        logging.info("No files in trash")
        return

    logging.info("Files in trash:")
    data = []
    for obj in objs:
        data.append(
            pack_in_row(
                user_path=obj['user_path'],
                path_to_restore=obj['path_to_restore'],
                removed_at=datetime.datetime.fromtimestamp(obj['removed_at']).strftime(
                    "%Y-%m-%d %H:%M:%S"),
                object_type=obj['type']))

        if obj['type'] == 'directory':
            files = get_files(trash.file_system,
                              obj['absolute_path_in_trash'],
                              max_depth=config.depth_view,
                              is_allow_file=lambda name, full_path: (
                                  re.search(config.regex_filter, name) is not None)
                              )
            if len(files) > 0:
                if files[0]['status'] == 'depth_limit_exceeded':
                    data.append(dir_not_empty_row(depth_level=1))
                if files[0]['status'] == 'permission_denied':
                    data.append(permission_denied_row(depth_level=1))

            for file_index, file in enumerate(files):
                if file_index == 0:
                    continue  # first is root directory

                if files[file_index - 1]['depth'] > file['depth']:
                    data.append(dir_end(file['depth']))

                full_path = file['path']
                if trash.file_system.is_directory(full_path):
                    file['name'] = file['name'] + '/'
                data.append(
                    pack_in_row(
                        depth_level=file['depth'],
                        path_to_restore=file['name'],
                        object_type=file['type']
                    )
                )
                if file['type'] == 'directory':
                    if file['status'] == 'depth_limit_exceeded':
                        data.append(dir_not_empty_row(depth_level=file['depth'] + 1))
                    if file['status'] == 'permission_denied':
                        data.append(permission_denied_row(depth_level=file['depth'] + 1))

        data.append([])  # empty row

    print(tabulate(headers=["Path in trash:", "Path to restore:", "Removed at:", "Type:"],
                   tabular_data=data
                   ))
