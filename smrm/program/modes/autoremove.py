import datetime
import time

from smrm.trash.trash import Trash
from smrm.config.config import AutoRemoveConfig


def autoremove_mode(config, trash):
    """
    :type config AutoRemoveConfig
    :type trash Trash
    :param config: 
    :param trash: 
    :return: 
    """
    datetime_before = datetime.datetime.now() - datetime.timedelta(days=config.days)
    time_before = time.mktime(datetime_before.timetuple())
    for descriptor in trash.get_descriptors():
        object_info = trash.object_info(descriptor)
        if object_info['removed_at'] < time_before:
            trash.erase_object(descriptor)
