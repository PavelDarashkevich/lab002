import logging

from smrm.config.config import OutputMode


def restore_mode(config, descriptors, trash):
    """
    :param descriptors: 
    :type config smrm.config.config.RestoreConfig
    :type trash Trash
    :param config: 
    :param trash: 
    :return: 
    """
    for descriptor in descriptors:
        trash.restore_file(descriptor, new_path=config.new_path)
        if config.output_mode <= OutputMode.ALL_INFO:
            logging.info("Descriptor %s has successfully processed!", descriptor)
