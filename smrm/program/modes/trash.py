import logging
from smrm.trash.trash import Trash

from smrm.config.config import OutputMode


class TrashPoliticsError(Exception):
    def __init__(self, message):
        super(TrashPoliticsError, self).__init__(message)
        self.message = message


def trash_mode(config, paths, trash):
    """
    :type config smrm.config.config.TrashConfig
    :type trash Trash
    
    :param config: 
    :param paths: 
    :param trash: 
    :return: 
    """
    for path in paths:
        descriptors_cnt = len(trash.get_descriptors())
        if descriptors_cnt >= config.max_descriptors_cnt:
            raise TrashPoliticsError('Descriptors count in trash exceeded')

        descriptor = trash.move_object(path)

        if config.output_mode <= OutputMode.ALL_INFO:
            if descriptor is None:
                logging.warning("Path %s hasn't moved to trash!", path)
            else:
                logging.info("Path %s moved to trash. Descriptor in trash: %s", path, descriptor)
