import logging

from smrm.config.config import OutputMode


def erase_mode(config, descriptors, trash):
    """
    :type config smrm.config.EraseConfig
    :type trash Trash
    :param config: 
    :param descriptors: 
    :param trash: 
    :return: 
    """
    for descriptor in descriptors:
        trash.erase_object(descriptor)
        if config.output_mode <= OutputMode.ALL_INFO:
            logging.info("Descriptor {descriptor} has processed!".format(descriptor=descriptor))
