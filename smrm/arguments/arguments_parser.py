from smrm.arguments.default_argparse import DefaultArgumentParser


class ArgumentsParser:
    def __init__(self):
        pass

    @staticmethod
    def _add_prompt_mode_to_parser(parser):
        """
        :type parser argparse.ArgumentParser
        :param parser: 
        :return: None
        """
        prompt_mode = parser.add_mutually_exclusive_group()
        prompt_mode.add_argument('-f', '--force', action='store_true', help='never prompt')
        prompt_mode.add_argument('-i', '--interactive', action='store_true',
                                 help='prompt before any operation')
        verbose_mode = parser.add_mutually_exclusive_group()
        verbose_mode.add_argument('-v', '--verbose', action='store_true',
                                  help='explain what is being done')
        verbose_mode.add_argument('-s', '--silent', action='store_true', help='nothing explain')

    @staticmethod
    def _add_view_files_parser(subparsers, parser):
        view_mode_parser = subparsers.add_parser(
            'view',
            help='view files in trash. All files have `id` that can be use to access to object')

        view_mode_parser.add_argument('-d', '--depth', type=int,
                                      help="max depth of view")
        view_mode_parser.add_argument('--regex', type=str,
                                      help='filter names by regex')

        descriptions_choose_mode = view_mode_parser.add_mutually_exclusive_group()
        descriptions_choose_mode.add_argument('--all', action='store_true',
                                              help="view all files")
        descriptions_choose_mode.add_argument('paths', nargs='*',
                                              help="path to files in trash", default=[])
        descriptions_choose_mode.required = True

        return view_mode_parser

    @staticmethod
    def _add_erase_parser(subparsers, parser):
        erase_mode_parser = subparsers.add_parser(
            'erase',
            help='erase objects from trash')
        g = erase_mode_parser.add_mutually_exclusive_group()
        g.add_argument('--all', action='store_true',
                       help="erase all files in trash")
        g.add_argument('descriptors', nargs='*',
                       help="must be dir, file or symlink. Other types don't supported", default=[])
        g.required = True
        erase_mode_parser.add_argument(
            "--regex",
            help="using regex to resolve names that should be erased")
        erase_mode_parser.add_argument('--dry_run', action='store_true')

        ArgumentsParser._add_prompt_mode_to_parser(erase_mode_parser)
        return erase_mode_parser

    @staticmethod
    def _add_restore_parser(subparsers, parser):
        restore_mode_parser = subparsers.add_parser(
            'restore',
            help="restore file or files by trash descriptions. Use --view_files for getting "
                 "descriptions")
        restore_mode_parser.add_argument(
            "--new_path",
            help="store files by this path")
        restore_mode_parser.add_argument(
            "--regex",
            help="using regex to resolve names that should be restored")
        restore_mode_parser.add_argument('--dry_run', action='store_true')

        g = restore_mode_parser.add_mutually_exclusive_group()
        g.add_argument('--all', action='store_true',
                       help="restore all files in trash")
        g.add_argument('descriptors', nargs='*',
                       help="must be dir, file or symlink. Other types don't supported", default=[])
        g.required = True

        ArgumentsParser._add_prompt_mode_to_parser(restore_mode_parser)
        return restore_mode_parser

    @staticmethod
    def _add_move_to_trash_parser(subparsers, parser):
        move_to_trash_parser = subparsers.add_parser(
            'trash',
            help='move all objects in trash. This is default behaviour')

        search_mode_group = move_to_trash_parser.add_argument_group('search mode')
        search_mode_group.add_argument('-d', '--dir', action='store_true', help='remove empty dirs')
        search_mode_group.add_argument('-r', '-R', '--recursive', action='store_true',
                                       help="search the contents of directories recursively.")
        search_mode_group.add_argument('--regex',
                                       help='provide regex')

        move_to_trash_parser.add_argument('--dry_run', action='store_true')
        move_to_trash_parser.add_argument(
            'paths', nargs='+',
            help="must be dir, file or symlink. Other types don't supported")

        ArgumentsParser._add_prompt_mode_to_parser(move_to_trash_parser)
        return move_to_trash_parser

    @staticmethod
    def _add_trash_repair_parser(subparsers, parser):
        repair_trash_parser = subparsers.add_parser(
            'repair',
            help='repair trash index'
        )

        ArgumentsParser._add_prompt_mode_to_parser(repair_trash_parser)
        return repair_trash_parser

    @staticmethod
    def _add_autoremove_parser(subparsers, parser):
        autoremove_parser = subparsers.add_parser(
            'autoremove',
            help='remove all descriptors that satisfied politics'
        )
        autoremove_parser.add_argument('--days')
        ArgumentsParser._add_prompt_mode_to_parser(autoremove_parser)
        return autoremove_parser

    def parse_arguments(self):
        parser = DefaultArgumentParser(
            prog='smrm',
            description='Smart RM: advanced remove files and directories utility for Linux',
        )

        parser.add_argument('-v', '--version', action='version', version='1.0',
                            help='output version information and exit')
        parser.add_argument('-c', '--config', type=str, dest='config_path',
                            help='path to config, must be a file or symlink')
        parser.add_argument('-t', '--trash', type=str, dest='trash_path',
                            help='path to trash, must be a directory')
        parser.add_argument('-l', '--log', type=str, dest='log_path',
                            help='path to log')

        subparsers = parser.add_subparsers(help='action mode', dest='action_mode')
        subparsers.required = True

        self._add_view_files_parser(subparsers, parser)
        self._add_erase_parser(subparsers, parser)
        self._add_restore_parser(subparsers, parser)
        self._add_move_to_trash_parser(subparsers, parser)
        self._add_trash_repair_parser(subparsers, parser)
        self._add_autoremove_parser(subparsers, parser)
        parser.set_default_subparser('trash')

        args = parser.parse_args()
        return args
