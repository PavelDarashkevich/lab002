import random
import re
import string
import time

from smrm.file_system import FileSystem, FileSystemPathNotFoundError
from smrm.trash.trash_database import TrashDatabase, TrashDatabaseRowNotExistError


class TrashError(Exception):
    def __init__(self, message):
        super(TrashError, self).__init__(message)


class TrashDirNotFoundError(TrashError):
    def __init__(self, path):
        super(TrashDirNotFoundError, self).__init__(path)
        self.path = path


class TrashInitError(TrashError):
    def __init__(self, message, **kwargs):
        super(TrashInitError, self).__init__(message)


class TrashDescriptorNotFoundError(TrashError):
    def __init__(self, descriptor, **kwargs):
        super(TrashDescriptorNotFoundError, self).__init__(descriptor)
        self.descriptor = descriptor


class TrashCorruptedError(TrashError):
    def __init__(self, descriptor, **kwargs):
        super(TrashCorruptedError, self).__init__(descriptor)
        self.descriptor = descriptor


class TrashProtectedPathError(TrashError):
    def __init__(self, path):
        super(TrashProtectedPathError, self).__init__(path)
        self.path = path


class Trash:
    def __init__(self, trash_path, file_system, protected_paths=None):
        self.lost_prefix = 'lost_'
        self.protected_paths = [trash_path]
        if protected_paths is not None:
            self.protected_paths.extend(protected_paths)

        self.file_system = file_system

        if not self.file_system.path_exists(trash_path):
            self.file_system.make_dir(trash_path, exist_ok=True)

        self.trash_path = trash_path

        self.db_path = FileSystem.join_path(trash_path, 'db.sqlite')
        self.database = TrashDatabase(self.db_path)

    def restore_file(self, user_path, new_path=None):
        """
        Restore file, dir or symlink
        :param user_path: 
        :param new_path: 
        :return: 
        """

        if user_path.startswith(self.lost_prefix):
            new_path = new_path or self.file_system.absolute_path('.')
            path_in_trash = self.file_system.join_path(self.trash_path, user_path)
            path_to_restore = self.file_system.join_path(new_path, user_path)

            self.file_system.move(path_in_trash, path_to_restore)
            return

        object_info = self.object_info(user_path)

        if not self.file_system.path_exists(object_info['absolute_path_in_trash']):
            raise TrashCorruptedError(user_path)

        path_to_restore = object_info['path_to_restore'] \
            if new_path is None else self.file_system.absolute_path(new_path)

        self.file_system.move(object_info['absolute_path_in_trash'], path_to_restore)

        full_path_to_root = self.file_system.join_path(self.trash_path,
                                                       object_info['root_name'])
        if not self.file_system.path_exists(full_path_to_root):
            self.database.delete(id=int(object_info['descriptor']))

        return path_to_restore

    def erase_object(self, user_path):
        """
        :param user_path: 
        :return: 
        """

        object_info = self.object_info(user_path)

        if not self.file_system.path_exists(object_info['absolute_path_in_trash']):
            raise TrashCorruptedError(user_path)

        self.file_system.remove(object_info['absolute_path_in_trash'])

        full_path_to_root = self.file_system.join_path(self.trash_path,
                                                       object_info['root_name'])
        if not self.file_system.path_exists(full_path_to_root):
            self.database.delete(id=object_info['descriptor'])

    def move_object(self, path):
        """
        Move dir, filename or symlink to trash
        :param path: 
        :raises
            FileSystemFileExistsError,
            FileSystemInvalidObject: object isn't dir, file or symlink,
            FileSystemDirNotEmpty,
            FileSystemIsADirectory
        :return: 
        """
        full_path_to_restore = self.file_system.absolute_path(path)

        if self._is_protected_path(full_path_to_restore):
            raise TrashProtectedPathError(path)

        name_in_trash = self.get_new_name_in_trash()

        object_db_info = self.database.create(
            path_to_restore=full_path_to_restore,
            name_in_trash=name_in_trash,
            removed_at=time.time()
        )

        absolute_path_in_trash = self.file_system.join_path(self.trash_path,
                                                            object_db_info['name_in_trash'])
        try:
            self.file_system.move(
                full_path_to_restore,
                absolute_path_in_trash)
        except Exception:
            if not self.file_system.path_exists(absolute_path_in_trash):
                self.database.delete(**object_db_info)
            raise

        if not self.file_system.path_exists(absolute_path_in_trash):
            self.database.delete(**object_db_info)
            return None
        return object_db_info['id']

    def object_info(self, user_path):
        """
        
        :param user_path: 
        :return: dict
        :type :return: dict
        """
        path_in_trash = self._convert_path(user_path)
        full_path_in_trash = self.file_system.join_path(self.trash_path, path_in_trash)

        if not self.file_system.path_exists(full_path_in_trash):
            raise FileSystemPathNotFoundError(user_path)

        descriptor, other_path = self._split_by_descriptor(user_path)
        db_object_info = self.database.get_object_info(id=int(descriptor))

        path_to_restore = db_object_info['path_to_restore']
        if other_path is not None:
            path_to_restore = self.file_system.join_path(
                db_object_info['path_to_restore'],
                other_path
            )

        answer = {
            'descriptor': descriptor,
            'name': self.file_system.base_name(path_to_restore),
            'root_name': db_object_info['name_in_trash'],

            'user_path': user_path,
            'path_to_restore': path_to_restore,
            'path_in_trash': path_in_trash,
            'absolute_path_in_trash': full_path_in_trash,
            'removed_at': db_object_info['removed_at'],
            'type': self.file_system.get_object_type(full_path_in_trash)
        }
        return answer

    def view_objects(self, paths):
        all_objects = []
        for rel_path in paths:
            all_objects.append(self.object_info(rel_path))

        return all_objects

    def repair(self):
        # self.database.database.begin()

        self.database.repair()

        broken_rows = []
        lost_objects = []

        for object_info in self.database.get_all():
            path = FileSystem.join_path(self.trash_path, object_info['name_in_trash'])
            if not self.file_system.path_exists(path):
                broken_rows.append(object_info)

        for obj in self.file_system.listdir(self.trash_path):
            if 'db.sqlite' in obj:
                continue

            if not self.database.exists(name_in_trash=obj):
                lost_objects.append(obj)

        for object_info in broken_rows:
            self.database.delete(**object_info)

        for lost_object in lost_objects:
            if lost_object.startswith(self.lost_prefix):
                continue

            random_string = ''.join(
                random.choice(string.ascii_uppercase + string.digits) for _ in range(10))

            self.file_system.move(
                self.file_system.join_path(self.trash_path, lost_object),
                self.file_system.join_path(self.trash_path,
                                           "{lost_prefix}{lost_object}_{random_string}".format(
                                               lost_prefix=self.lost_prefix, lost_object=lost_object,
                                               random_string=random_string))
            )

        # self.database.database.commit()

        return {
            'broken_rows': broken_rows,
            'lost_objects': [obj for obj in self.file_system.listdir(self.trash_path) if
                             obj.startswith('lost_')]
        }

    def get_descriptors(self):
        return [str(object_info['id']) for object_info in self.database.get_all()]

    def is_empty(self):
        return self.database.is_empty()

    @staticmethod
    def _split_by_descriptor(path_in_trash):
        if path_in_trash is None:
            return None, None

        a = re.split(r'[/\\]', path_in_trash, maxsplit=1)
        if len(a) == 0:
            return None, None

        if len(a) == 1:
            if a[0] is None or len(a[0].strip()) == 0:
                return None, None
            return a[0], None

        if a[0] is None or len(a[0].strip()) == 0:
            return None, a[1]
        return a[0].strip(), a[1]

    def get_new_name_in_trash(self):
        for i in range(10):
            new_descriptor = self.get_random_descriptor()

            if not self.file_system.path_exists(
                    self.file_system.join_path(
                        self.trash_path,
                        new_descriptor
                    )
            ):
                return new_descriptor

        raise RuntimeError()

    @staticmethod
    def get_random_descriptor():
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))

    def _convert_path(self, user_path):
        descriptor, other_path = self._split_by_descriptor(user_path)

        descriptor = descriptor.strip()
        if not re.match("[0-9]+", descriptor):
            raise TrashDescriptorNotFoundError(descriptor)

        try:
            object_info = self.database.get_object_info(id=int(descriptor))
        except TrashDatabaseRowNotExistError:
            raise TrashDescriptorNotFoundError(descriptor)

        return self.file_system.join_path(object_info['name_in_trash'], other_path or "")

    def _is_protected_path(self, path):
        path = self.file_system.absolute_path(path)
        if not path.endswith(self.file_system.path_separator):
            path = path + self.file_system.path_separator
        for protected_path in self.protected_paths:
            protected_path = self.file_system.absolute_path(protected_path)
            if not protected_path.endswith(self.file_system.path_separator):
                protected_path = protected_path + self.file_system.path_separator

            if protected_path.startswith(path):
                return True
            if path.startswith(protected_path):
                return True
        return False
