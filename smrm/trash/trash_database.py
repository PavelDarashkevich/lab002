import os

from peewee import Model, TextField, IntegerField, SqliteDatabase
from playhouse.shortcuts import model_to_dict


class TrashDatabaseError(Exception):
    pass


class TrashDatabaseInitError(TrashDatabaseError):
    pass


class TrashDatabaseRowNotExistError(TrashDatabaseError):
    pass


class TrashDatabase(object):
    class TrashFilesModel(Model):
        path_to_restore = TextField()
        name_in_trash = TextField()
        removed_at = IntegerField()

        class Meta:
            database = None
            db_table = 'files'

    def __init__(self, path):
        self.path = path
        try:
            self.database = SqliteDatabase(path, pragmas=[('journal_mode', 'wal')])
            setattr(self.TrashFilesModel._meta, "database", self.database)
        except Exception:
            raise TrashDatabaseInitError(path)

    def get_object_info(self, **kwargs):
        try:
            return model_to_dict(self.TrashFilesModel.get(**kwargs))
        except self.TrashFilesModel.DoesNotExist:
            raise TrashDatabaseRowNotExistError

    def get_all(self):
        return (model_to_dict(row) for row in self.TrashFilesModel.select())

    def is_empty(self):
        return self.TrashFilesModel.select().count() == 0

    def create(self, **kwargs):
        row = self.TrashFilesModel.create(**kwargs)
        row.save()
        return model_to_dict(row)

    def delete(self, **kwargs):
        self.TrashFilesModel.get(**kwargs).delete_instance()

    def exists(self, **kwargs):
        try:
            _ = self.TrashFilesModel.get(**kwargs)
            return True
        except self.TrashFilesModel.DoesNotExist:
            return False

    def repair(self):
        if not os.path.exists(os.path.dirname(self.path)):
            os.mkdir(os.path.dirname(self.path))

        self.TrashFilesModel.create_table(True)
