import os
import sys
import enum
import logging
from smrm.config.config import Config, ActionMode

if sys.version_info[0] == 2:
    get_input = raw_input
else:
    get_input = input


class UserAnswer(enum.Enum):
    OK = 'Ok'
    CANCEL = 'Cancel'
    YES = 'Yes'
    NO = 'No'

    ABORT = 'Abort'
    RETRY = 'Retry'
    IGNORE = 'Ignore'

    CONTINUE = 'Continue'

    MERGE = 'Merge'
    REPLACE = 'Replace'
    SKIP = 'Skip'
    RENAME = 'Rename'


class Action(enum.Enum):
    LIST_DIR = 'list_dir'
    MOVE = 'move'
    REMOVE = 'remove'
    COPY = 'copy'
    DESCEND_INTO = 'descend_into'

    NEW_NAME = 'new_name'


class ActionFlag(enum.Enum):
    NO_FLAG = 'no_flag'
    ALL = 'all'
    CURRENT_DIR = 'current_dir'
    ALL_TREE = 'all_tree'


class UserQuestions(object):
    def __init__(self, config):
        """
        :type config Config
        :param config: 
        """
        self.config = config
        self.memo_flags = {}

    def _is_valid_memo(self, name, path):
        user_answer, flag, old_path = self.memo_flags.get(
            name,
            (UserAnswer.ABORT, ActionFlag.NO_FLAG, None))

        if flag == ActionFlag.NO_FLAG:
            return False
        elif flag == ActionFlag.ALL:
            return True
        elif flag == ActionFlag.CURRENT_DIR:
            return os.path.dirname(old_path) == os.path.dirname(path)
        elif flag == ActionFlag.ALL_TREE:
            return os.path.dirname(path).startswith(os.path.dirname(old_path))

        return False

    def permission_denied(self, action, path):
        if self._is_valid_memo('permission_denied', path):
            return self.memo_flags['permission_denied'][0]

        if self.config.action_mode in [ActionMode.FAIL, ActionMode.FORCE]:
            return UserAnswer.ABORT
        if self.config.action_mode == ActionMode.SKIP:
            return UserAnswer.SKIP
        if self.config.action_mode == ActionMode.ASK:
            logging.error("Permission denied on action: %s, path: %s", action.value, path)

            answer, flag = self.user_question([UserAnswer.RETRY, UserAnswer.SKIP, UserAnswer.ABORT])
            self.memo_flags['permission_denied'] = (answer, flag, path)

            logging.debug("User answer: %s", answer.value)
            return answer

        raise NotImplementedError

    def path_exists(self, action, src, dst, is_directories=False):
        if self._is_valid_memo('path_exists', src):
            return self.memo_flags['path_exists'][0]

        if self.config.action_mode == ActionMode.FAIL:
            return UserAnswer.ABORT
        if self.config.action_mode == ActionMode.FORCE:
            return UserAnswer.MERGE if is_directories else UserAnswer.REPLACE
        if self.config.action_mode == ActionMode.SKIP:
            return UserAnswer.SKIP
        if self.config.action_mode == ActionMode.ASK:
            actions = [UserAnswer.RETRY, UserAnswer.SKIP, UserAnswer.ABORT, UserAnswer.REPLACE,
                       UserAnswer.RENAME]
            if is_directories:
                actions.append(UserAnswer.MERGE)

            logging.error("Path exists: %s, source path: %s", dst, src)
            answer, flag = self.user_question(actions)
            self.memo_flags['path_exists'] = (actions, flag, src)
            logging.debug("User answer: %s", answer.value)

            return answer

        raise NotImplementedError

    def new_name(self, action, src):
        new_name = self.user_get_line('Enter new name: ')
        logging.debug("User input: %s", new_name)

        return new_name
    
    def move_to_trash(self, action, src):
        if self._is_valid_memo('move_to_trash', src):
            return self.memo_flags['move_to_trash'][0]

        if self.config.action_mode != ActionMode.ASK:
            return UserAnswer.YES

        actions = [UserAnswer.YES, UserAnswer.SKIP, UserAnswer.ABORT]
        logging.info("Do you want to move '%s' to trash?", src)
        answer, flag = self.user_question(actions)
        self.memo_flags['move_to_trash'] = (answer, flag, src)
        logging.debug(answer.value)

        return answer

    def descend_into(self, action, src):
        if self._is_valid_memo('descend_into', src):
            return self.memo_flags['descend_into'][0]

        actions = [UserAnswer.YES, UserAnswer.NO, UserAnswer.ABORT]

        if self.config.action_mode != ActionMode.ASK:
            return UserAnswer.YES

        logging.info("Descend into '%s'?", src)
        answer, flag = self.user_question(actions)
        self.memo_flags['descend_into'] = (answer, flag, src)
        logging.debug(answer.value)

        return answer

    @staticmethod
    def user_get_line(message):
        try:
            return get_input(message)
        except (KeyboardInterrupt, EOFError):
            return UserAnswer.ABORT

    @staticmethod
    def user_question(choices):
        while True:
            try:
                raw_answer = get_input("/".join(choice.value for choice in choices) +
                                       ": -a=all, -c='in current dir', -t='in subtree': ").strip()
                tokens = raw_answer.split(' ')
                answer = tokens[0]
                if len(tokens) > 2:
                    print('Incorrect input.')
                    continue

                if len(tokens) == 1:
                    flag = ActionFlag.NO_FLAG
                elif tokens[1] == '-a':
                    flag = ActionFlag.ALL
                elif tokens[1] == '-c':
                    flag = ActionFlag.CURRENT_DIR
                elif tokens[1] == '-t':
                    flag = ActionFlag.ALL_TREE
                else:
                    print('Invalid flag: %s' % tokens[1])
                    continue

            except (KeyboardInterrupt, EOFError):
                return UserAnswer.ABORT, ActionFlag.NO_FLAG

            probable_answers = []
            for choice in choices:
                if choice.value.lower().startswith(answer.lower()):
                    probable_answers.append(choice)

            if len(probable_answers) == 1:
                return probable_answers[0], flag
            print('Incorrect choice: {answer}'.format(answer=answer))
