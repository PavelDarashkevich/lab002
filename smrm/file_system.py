import os
import shutil

import sys
if sys.version_info[0] == 2:
    IsADirectoryError = OSError
    FileExistsError = OSError


class FileSystemInvalidObjectError(Exception):
    def __init__(self, path):
        super(FileSystemInvalidObjectError, self).__init__(path)
        self.path = path


class FileSystemPathNotFoundError(Exception):
    def __init__(self, path):
        super(FileSystemPathNotFoundError, self).__init__(path)
        self.path = path


class FileSystemDirNotEmpty(IsADirectoryError):
    def __init__(self, path):
        super(FileSystemDirNotEmpty, self).__init__(path)
        self.path = path


class FileSystemIsADirectoryError(IsADirectoryError):
    def __init__(self, path):
        super(FileSystemIsADirectoryError, self).__init__(path)
        self.path = path


class FileSystemPathExistsError(FileExistsError):
    def __init__(self, path):
        super(FileSystemPathExistsError, self).__init__(path)
        self.path = path


class FileSystemPermissionDeniedError(OSError):
    def __init__(self, path):
        super(FileSystemPermissionDeniedError, self).__init__(path)
        self.path = path


class FileSystem:
    path_separator = os.path.sep

    def __init__(self, dry_mode=False):
        self.dry_mode = dry_mode
        self.changes = {}

    def is_directory(self, path):
        return not os.path.islink(path) and os.path.isdir(path)

    def is_symlink(self, path):
        return os.path.islink(path)

    def is_file(self, path):
        return not os.path.islink(path) and os.path.isfile(path)

    def path_exists(self, path):
        if self.is_symlink(path):  # os.path.exists return False for broken symlinks
            return True
        return os.path.exists(path)

    @staticmethod
    def absolute_path(path):
        return os.path.abspath(os.path.expandvars(os.path.expanduser(path)))

    def remove(self, path):
        """
        Remove path without restore
        :param path: path to remove
        :return: None
        :raises: NotImplementedError()
        """
        if self.dry_mode:
            return

        if not self.path_exists(path):
            raise FileSystemPathNotFoundError(path)

        if self.is_symlink(path) or self.is_file(path):
            self.unlink(path)
            return
        elif not self.is_directory(path):
            raise FileSystemInvalidObjectError(path)

        # if is directory
        objs = self.listdir(path)

        for obj in objs:
            self.remove(self.join_path(path, obj))

        self.remove_empty_dir(path)

    def copy(self, src, dst):
        """
        Copy file, dir or symlink from `src` to `dst`
        :param src: source path
        :param dst: destination path
        :return: None
        """
        if self.dry_mode:
            return

        if not self.path_exists(src):
            raise FileSystemPathNotFoundError(src)

        if self.path_exists(dst):
            raise FileSystemPathExistsError(dst)

        parent_dir = self.parent_dir(dst)
        self.make_dir(parent_dir, exist_ok=True)

        if self.is_symlink(src):
            self.make_symlink(self.read_symlink(src), dst)
            return
        elif self.is_file(src):
            shutil.copyfile(src, dst)
            return
        elif not self.is_directory(src):
            raise FileSystemInvalidObjectError(src)

        # if is directory
        self.make_dir(dst, exist_ok=True)  # if dir is empty
        objs = self.listdir(src)
        for obj in objs:
            self.copy(
                self.join_path(src, obj),
                self.join_path(dst, obj),
            )

    def move(self, src, dst):
        """
        Move file, dir or symlink from `src` to `dst`. First try fast move (rename). If it fails,
        move through copy-remove
        :param src: source path
        :param dst: source path
        :raises
            FileExistsError,
            FileSystemInvalidObject: object isn't dir, file or symlink,
            FileSystemDirNotEmpty,
            FileSystemIsADirectory
        :return: None
        """

        if self.dry_mode:
            return

        if not self.path_exists(src):
            raise FileSystemPathNotFoundError(src)

        if self.path_exists(dst):
            raise FileSystemPathExistsError(dst)

        parent_dir = self.parent_dir(dst)
        self.make_dir(parent_dir, exist_ok=True)

        if self.is_file(src):
            try:
                self.rename(src, dst)
            except OSError:
                self.copy(src, dst)
                self.remove(src)
            return
        elif self.is_symlink(src):
            linkto = self.read_symlink(src)
            self.make_symlink(linkto, dst)
            self.unlink(src)
            return
        elif not self.is_directory(src):
            raise FileSystemInvalidObjectError(src)

        # if is directory
        try:
            self.rename(src, dst)
        except OSError:
            objs = self.listdir(src)
            self.make_dir(dst, exist_ok=True)
            for obj in objs:
                self.move(
                    self.join_path(src, obj),
                    self.join_path(dst, obj),
                )
            self.remove_empty_dir(src)

    def get_object_type(self, path):
        if self.is_symlink(path):
            return 'symlink'
        if self.is_file(path):
            return 'file'
        if self.is_directory(path):
            return 'directory'
        return None

    @staticmethod
    def join_path(path, subpath):
        if os.path.isabs(subpath):
            return subpath
        if subpath is None or len(subpath) == 0:
            return path

        return os.path.join(path, subpath)

    def make_dir(self, path, exist_ok=False):
        if self.dry_mode:
            return

        if exist_ok and os.path.isdir(path):
            return

        if os.path.exists(path):
            raise FileSystemPathExistsError(path)

        os.makedirs(path)

    @staticmethod
    def parent_dir(path):
        return os.path.dirname(path)

    @staticmethod
    def relative_path(path, src):
        return os.path.relpath(path, src)

    def walk(self, top, topdown=True, onerror=None, followlinks=False):
        if self.dry_mode:
            return

        return os.walk(top, topdown=topdown, onerror=onerror, followlinks=followlinks)

    def rename(self, src, dst):
        if self.dry_mode:
            return

        try:
            return os.rename(src, dst)
        except OSError as e:
            if e.errno == 13:  # permission denied
                raise FileSystemPermissionDeniedError(e.filename)
            raise e

    def read_symlink(self, path):
        try:
            return os.readlink(path)
        except OSError as e:
            if e.errno == 13:  # permission denied
                raise FileSystemPermissionDeniedError(e.filename)
            raise e

    def make_symlink(self, linkto, dst):
        if self.dry_mode:
            return

        return os.symlink(linkto, dst)

    def unlink(self, path):
        if self.dry_mode:
            return

        try:
            os.unlink(path)
        except OSError as e:
            if e.errno == 13:  # permission denied
                raise FileSystemPermissionDeniedError(e.filename)
            raise e

    def remove_empty_dir(self, path):
        if self.dry_mode:
            return

        try:
            os.rmdir(path)
        except OSError as e:
            if e.errno == 13:  # permission denied
                raise FileSystemPermissionDeniedError(e.filename)
            elif e.errno == 39:
                raise FileSystemDirNotEmpty(e.filename)
            raise e

    @staticmethod
    def split(path):
        return os.path.split(path)

    def listdir(self, path):
        try:
            return os.listdir(path)
        except OSError:
            raise FileSystemPermissionDeniedError(path)

    @staticmethod
    def base_name(path):
        return os.path.basename(path)
