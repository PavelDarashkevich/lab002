import enum
import os

import sys
from abc import abstractmethod

if sys.version_info[0] == 2:
    FileNotFoundError = OSError


def _get_abs_path(path, base):
    return os.path.abspath(os.path.join(base, os.path.expandvars(os.path.expanduser(path))))


class ConfigError(Exception):
    pass


class ConfigNotFoundError(ConfigError, FileNotFoundError):
    def __init__(self, message):
        super(ConfigNotFoundError, self).__init__(message)


class ConfigInvalidFormat(ConfigError):
    def __init__(self, message):
        super(ConfigInvalidFormat, self).__init__(self, message)


class ActionMode(enum.Enum):
    FAIL = 0
    SKIP = 1
    ASK = 2
    FORCE = 3


class OutputMode(enum.IntEnum):
    DEBUG = 10
    ALL_INFO = 15
    MAIN_INFO = 20
    WARNING = 30
    ERROR = 40
    CRITICAL = 50
    NO_OUTPUT = 100


class Config(object):
    def __init__(self):
        self.config_path = os.path.expanduser('~/.smrm/config.json')
        self.trash_path = os.path.expanduser('~/.smrm/trash')
        self.log_path = os.path.expanduser('~/.smrm/smrm.log')
        self.protected_paths = []

        self.dry_mode = False

        self.action_mode = ActionMode.ASK
        self.output_mode = OutputMode.MAIN_INFO

    @abstractmethod
    def load_from_json(self, json_data):
        """
        :type json_data: dict
        :param json_data: 
        :return: 
        """
        config_parent_dir = os.path.dirname(self.config_path)

        self.trash_path = json_data.get('trash_path') or self.trash_path
        if not os.path.isabs(self.trash_path):
            self.trash_path = _get_abs_path(self.trash_path, config_parent_dir)
        self.log_path = json_data.get('log_path') or self.log_path
        if not os.path.isabs(self.log_path):
            self.log_path = _get_abs_path(self.log_path, config_parent_dir)

        for path in json_data.get('protected_paths', []):
            self.protected_paths.append(_get_abs_path(path, config_parent_dir))

        self.dry_mode = json_data.get('dry_mode') or self.dry_mode

        if json_data.get('action_mode'):
            self.action_mode = ActionMode[json_data.get('action_mode').upper()]
        if json_data.get('output_mode'):
            self.output_mode = OutputMode[json_data.get('output_mode').upper()]
        return self


class ViewConfig(Config):

    class ViewMode(enum.Enum):
        TREE = 1
        TABLE = 2

    def __init__(self):
        super(ViewConfig, self).__init__()

        self.depth_view = -1  # max depth
        self.regex_filter = ""

    def load_from_json(self, json_data):
        super(ViewConfig, self).load_from_json(json_data)

        view_data = json_data.get('view')
        if view_data is None:
            return self

        if view_data.get('depth_view') is not None:
            self.depth_view = view_data.get('depth_view')
        self.regex_filter = view_data.get('regex_filter') or self.regex_filter

        return self


class RestoreConfig(Config):
    def __init__(self):
        super(RestoreConfig, self).__init__()

        self.new_path = None
        self.regex_filter = ""

    def load_from_json(self, json_data):
        super(RestoreConfig, self).load_from_json(json_data)

        restore_data = json_data.get('restore')
        if restore_data is None:
            return self

        self.new_path = json_data.get('new_path') or self.new_path
        self.regex_filter = json_data.get('regex_filter') or self.regex_filter

        return self


class EraseConfig(Config):
    def __init__(self):
        super(EraseConfig, self).__init__()
        self.regex_filter = ""

    def load_from_json(self, json_data):
        super(EraseConfig, self).load_from_json(json_data)

        erase_data = json_data.get('erase', dict())
        self.regex_filter = erase_data.get('regex_filter') or self.regex_filter
        return self


class TrashConfig(Config):
    def __init__(self):
        super(TrashConfig, self).__init__()

        self.move_empty_dirs = False
        self.recursive = False
        self.regex_filter = ""
        self.max_descriptors_cnt = 1000

    def load_from_json(self, json_data):
        super(TrashConfig, self).load_from_json(json_data)

        trash_data = json_data.get('trash', dict())
        self.move_empty_dirs = trash_data.get('move_empty_dirs') or self.move_empty_dirs
        self.recursive = trash_data.get('recursive') or self.recursive
        self.regex_filter = trash_data.get('regex_filter') or self.regex_filter

        self.max_descriptors_cnt = int(
            trash_data.get('max_descriptors_cnt', self.max_descriptors_cnt))

        return self


class TrashRepairConfig(TrashConfig):
    def __init__(self):
        super(TrashRepairConfig, self).__init__()

    def load_from_json(self, json_data):
        super(TrashRepairConfig, self).load_from_json(json_data)

        return self


class AutoRemoveConfig(Config):
    def __init__(self):
        super(AutoRemoveConfig, self).__init__()

        self.days = 365

    def load_from_json(self, json_data):
        super(AutoRemoveConfig, self).load_from_json(json_data)

        autoremove_data = json_data.get('autoremove', dict())
        self.days = int(autoremove_data.get('days', self.days))

        return self
