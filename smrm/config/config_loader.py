import json
import os

from smrm.config.config import ViewConfig, RestoreConfig, EraseConfig, TrashConfig, \
    TrashRepairConfig, Config, OutputMode, ActionMode, AutoRemoveConfig


class ConfigLoader(object):
    def __init__(self):
        self.config_mode = {
            'view': ViewConfig,
            'restore': RestoreConfig,
            'erase': EraseConfig,
            'trash': TrashConfig,
            'repair': TrashRepairConfig,
            'autoremove': AutoRemoveConfig
        }

        self.update_mode = {
            'view': self._update_view,
            'restore': self._update_restore,
            'erase': self._update_erase,
            'trash': self._update_trash,
            'repair': self._update_repair,
            'autoremove': self._update_autoremove
        }

    def load(self, arguments, config_path='~/.smrm/config.json'):
        default_config_path = os.path.expandvars(os.path.expanduser(config_path))
        config_path = arguments.config_path or default_config_path

        with open(config_path) as config_file_handler:
            json_data = json.load(config_file_handler)

        config = self.config_mode.get(
            arguments.action_mode, Config)().load_from_json(json_data)

        self.update_mode.get(arguments.action_mode, self._update_default)(config, arguments)
        return config

    @staticmethod
    def _update_default(config, arguments):
        """
        :type config Config
        :param config: 
        :param arguments: 
        :return: 
        """
        if arguments.log_path is not None:
            config.log_path = os.path.join(os.getcwd(), arguments.log_path)
        if arguments.config_path is not None:
            config.config_path = os.path.join(os.getcwd(), arguments.config_path)
        if arguments.trash_path is not None:
            config.trash_path = os.path.join(os.getcwd(), arguments.trash_path)

        config.dry_mode = getattr(arguments, 'dry_run', config.dry_mode)

        if getattr(arguments, 'silent', False):
            config.output_mode = OutputMode.NO_OUTPUT
            config.action_mode = ActionMode.SKIP

        if getattr(arguments, 'verbose', False):
            config.output_mode = OutputMode.ALL_INFO

        if getattr(arguments, 'force', False):
            config.action_mode = ActionMode.FORCE

        if getattr(arguments, 'interactive', False):
            config.action_mode = ActionMode.ASK

    @staticmethod
    def _update_view(config, arguments):
        """
        :type config: ViewConfig
        """
        ConfigLoader._update_default(config, arguments)

        if arguments.depth is not None:
            config.depth_view = arguments.depth
        config.regex_filter = arguments.regex or config.regex_filter

        return config

    @staticmethod
    def _update_restore(config, arguments):
        """
        :type config RestoreConfig
        :param config: 
        :param arguments: 
        :return: 
        """
        ConfigLoader._update_default(config, arguments)

        config.new_path = arguments.new_path or config.new_path
        config.regex_filter = arguments.regex or config.regex_filter

    @staticmethod
    def _update_erase(config, arguments):
        """
        :type config EraseConfig
        :param config: 
        :param arguments: 
        :return: 
        """
        ConfigLoader._update_default(config, arguments)

        config.regex_filter = arguments.regex or config.regex_filter

    @staticmethod
    def _update_trash(config, arguments):
        """
        :type config TrashConfig
        :param config:
        :param arguments:
        :return:
        """
        ConfigLoader._update_default(config, arguments)

        config.move_empty_dirs = arguments.dir or config.move_empty_dirs
        config.recursive = arguments.recursive or config.recursive
        config.regex_filter = arguments.regex or config.regex_filter

    @staticmethod
    def _update_repair(config, arguments):
        ConfigLoader._update_default(config, arguments)

    @staticmethod
    def _update_autoremove(config, arguments):
        """
        :type config AutoRemoveConfig
        :param config: 
        :param arguments: 
        :return: 
        """
        ConfigLoader._update_default(config, arguments)

        if arguments.days:
            config.days = arguments.days
